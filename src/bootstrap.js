'use strict';

let Cc = Components.classes
    , Ci = Components.interfaces
    ;

function startup(data, reason) {
    // only let this functionality run when extension is started during firefox startup
    if (reason != APP_STARTUP) return;
    
    let windowMediator = Cc["@mozilla.org/appshell/window-mediator;1"]
        .getService(Ci.nsIWindowMediator);
    
    // listen for first window creation
    let windowListener = {
        onOpenWindow: function (aWindow) {
        
            let domWindow = aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
            
            // wait for it to be ready
            domWindow.addEventListener("load", function listenerFunc() {
                // remove listeners = make sure this is done only once
                windowMediator.removeListener(windowListener);
                domWindow.removeEventListener("load", listenerFunc);
                
                domWindow.gBrowser.ownerGlobal.addEventListener('activate', function activateListener() {
                    // remove listeners = make sure this is done only once
                    domWindow.gBrowser.ownerGlobal.removeEventListener('activate', activateListener);
                    
                    // show browser console
                    domWindow.document.getElementById('menu_browserConsole').click();
                }, false);
                
                /* ALTERNATIVE #1
                domWindow.document.getElementById('menu_browserConsole').click();
                */

                /* ALTERNATIVE #2
                gBrowser.ownerGlobal
                    .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                    .getInterface(Components.interfaces.nsIWebNavigation)
                    .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                    .rootTreeItem
                    .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                    .getInterface(Components.interfaces.nsIDOMWindow)
                    .document.getElementById('toolbar-menubar')
                */
                
                /* ALTERNATIVE #3
                gBrowser.ownerGlobal.document.getElementById('menu_browserConsole').click();
                */
                
                /* ALTERNATIVE #4
                domWindow.HUDService.toggleBrowserConsole();
                */
            });
            
        },
        onCloseWindow: function (aWindow) { },
        onWindowTitleChange: function (aWindow, aTitle) { }
    };
    
    windowMediator.addListener(windowListener);
}

// no need for cleanup, since this extension doesn't modify anything
function shutdown(data, reason) { }
function install(data, reason) { }
function uninstall(data, reason) { }
