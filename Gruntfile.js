/*
    # produce xpi:
    $ npm install
    $ npm install -g grunt-cli
    $ grunt
*/

module.exports = function(grunt) {

    grunt.initConfig({
        compress: {
          main: {
            options: {
              archive: 'dist/autoOpenBrowserConsole.xpi',
              mode: 'zip'
            },
            files: [
              {expand:true, cwd:'src/', src: ['**']},
              {src: ['LICENSE.TXT']}
            ]
          }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-compress');
    
    grunt.registerTask('default', ['compress']);
}
